QmlToy
======

** NB `main.cpp` maybe hardwired to load something other than `index.qml` **

A minimal host for QML files, mainly intended for demonstrating iOS specific issues.

Basically all it is, is:

* A QQuickView subclass (Browser) which:
    * Exposes itself to the hosted QML as object ID `browser`.
    * Adds a load slot which can be used to switch to another QML page e.g `browser.load("video.qml")`.
* Some QML files.  The one loaded by default is `index.qml` which includes `QtQuick.Dialogs`'s `FileDialog` logic to switch to other QML files.

If qmltoy is given any commandline arguments, the last one will be treated as a filename to load, otherwise `index.qml` is loaded by default.

Instructions
------------

* Build using appropriate `./MAKE-linux`, `./MAKE-mac`, `./MAKE-ios`; NB `export PATH=...` will likely need updating to your install location if not `${PWD}`
* Obtain `sample_iTunes.mov` (640x480 video) from <http://support.apple.com/en-us/HT201549> and put it in this directory.
* On Linux: `./build/qmltoy`
* On Mac: `./build/qmltoy.app/Contents/MacOS/qmltoy`
* On iOS:
    * For ios, to open in xcode, will need to create a qmltoy.xcworkspace to open the project in.
    * Launch from xcode onto your iOS device
    * It'll log it can't find `Documents/index.qml`
    * Use iTunes to upload `sample_iTunes.mov` and *all* the `*.qml` to the app's `Documents/` folder.
    * Restart the app on the device.
* By default, the application will open with the `index.qml`; that includes a button to bring up a file dialog (hard to use on touch!) from where another file can be selected `video.qml`.

Sources
-------
glow.qml and butterfly.png from http://doc.qt.io/qt-5/qml-qtgraphicaleffects-glow.html and Qt sources.

Bug Demonstrator
----------------
This code has been used as a demonstrator for:

* [QTBUG-47390](https://bugreports.qt.io/browse/QTBUG-47390); tagged `qtbug-47390` in this repo
* [QTBUG-42840](https://bugreports.qt.io/browse/QTBUG-42840); tagged `qtbug-42840` in this repo.
* [QTBUG-42721](https://bugreports.qt.io/browse/QTBUG-42721); tagged `qtbug-42721` in this repo.

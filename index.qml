import QtQuick 2.2
import QtQuick.Controls 1.2
import QtQuick.Dialogs 1.0

Rectangle {
  id: main
  anchors.fill: parent
  color: '#000000'

  Text {
    id: title
    anchors.horizontalCenter: parent.horizontalCenter
    anchors.top: parent.top
    text: "QMLToy"
    font.pixelSize: 0.175*main.height
    color: '#ffff00'
  }

  Text {
    anchors.horizontalCenter: parent.horizontalCenter
    anchors.top: title.bottom
    text: "Minimal QML browser (QQuickView host)\nThis is file index.qml"
    horizontalAlignment: Text.AlignHCenter
    font.pixelSize: 0.05*main.height
    color: '#ff8800'
  }

  Button {
    anchors.horizontalCenter: parent.horizontalCenter
    anchors.verticalCenter: parent.verticalCenter
    width: parent.width/4
    height: width/3
    text: "Select a QML file..."
    action: Action {onTriggered: chooser.visible=true;}
  }

  FileDialog {
    id: chooser
    title: "Choose a QML file"
    folder: "."
    nameFilters: ["QML files (*.qml)","All files (*)"]
    selectExisting: true
    selectMultiple: false
    onAccepted: {
      console.log('Selected file "'+chooser.fileUrls[0]+'"')
      browser.load(chooser.fileUrls[0]);
    }
  }
}

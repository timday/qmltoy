#include <QDir>
#include <QGuiApplication>
#include <QStandardPaths>
#include <QUrl>

#include "Browser.h"

int main(int argc,char* argv[]) {
  
  QGuiApplication app(argc,argv);

#ifdef IOSQUIRKS
  // On iOS, change current directory to apps' Documents/ directory
  QDir::setCurrent(
    QStandardPaths::standardLocations(
      QStandardPaths::DocumentsLocation
    )[0]
  );
#endif

  // Browser is not much more than a QQuickView
  Browser browser;

  // On Desktop platforms, set an explicit window size
  // (Harmless on iOS which will just fullscreen it anyway)
  browser.resize(QSize(640,480));
  browser.setResizeMode(QQuickView::SizeRootObjectToView);

  browser.show();

  // Start with file "index.qml" unless commandline argument(s) is provided.
  // (Note: the provided index.qml includes a file-browser control)

  QString filename="index.qml";

  if (QGuiApplication::arguments().size()>1) {
    filename=QGuiApplication::arguments().back();
  }
  browser.setSource(QUrl::fromLocalFile(filename));

  return app.exec();
}

import QtQuick 2.0
import QtGraphicalEffects 1.0

Rectangle {
  // Size will be overridden by host anyway.
  width: 640
  height: 480

  color: "black"

  Image {
     id: butterfly
     anchors.centerIn: parent
     width: 300
     height: 300
     source: "butterfly.png"
     visible: false
  }

  Glow {
    anchors.fill: butterfly
    source: butterfly
    radius: 16
    samples: 32
    color: "#0098db"
  }
}

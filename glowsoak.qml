import QtQuick 2.0
import QtGraphicalEffects 1.0

Item {
  width: 300
  height: 300

  Rectangle {
    anchors.fill: parent
    color: "black"
  }

  Image {
    id: butterfly
    source: "butterfly.png"
    sourceSize: Qt.size(parent.width, parent.height)
    smooth: true
    visible: false
  }

  Glow {
    anchors.fill: butterfly
    radius: 8
    samples: 16
    color: "white"
    source: butterfly
  }

  Timer {
    interval: 250
    running: true
    onTriggered: browser.load("glowsoak.qml")
  }

}

import QtQuick 2.2
import QtQuick.Controls 1.2

Rectangle {
  id: main
  anchors.fill: parent
  color: '#000000'

  Rectangle {
    id: header
    anchors.top: main.top
    height: parent.height/8
    width: parent.width
    color: '#00cc00'
    Text {
      anchors.fill: parent
      horizontalAlignment: Text.AlignHCenter
      verticalAlignment: Text.AlignVCenter
      text: "notvideo.qml: "+main.width+"x"+main.height
      color: '#000000'
    }
  }

  Rectangle {
    id: footer
    anchors.bottom: main.bottom
    height: parent.height/8
    width: parent.width
    color: '#00cc00'
    Text {
      anchors.fill: parent
      horizontalAlignment: Text.AlignHCenter
      verticalAlignment: Text.AlignVCenter
      text: "No video to be seen here"
      color: '#000000'
    }

    Button {
      x: 0.1*parent.width-0.5*width
      anchors.verticalCenter: parent.verticalCenter
      id: home
      text: "Index"
      action: Action {onTriggered: browser.load("index.qml");}
    }

    Button {
      x: 0.9*parent.width-0.5*width
      anchors.verticalCenter: parent.verticalCenter
      id: notvideo
      text: "Video"
      action: Action {onTriggered: browser.load("video.qml");}
    }
  }
  
}

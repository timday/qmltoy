Screenshots from loading glow.qml in the trivial qmltoy app.

NB on iOS, once the app is installed, need to load index.qml, butterfly.png and glow.qml via itunes to qmltoy's documents, then (re)run the app; it should find index.qml which displays a file-chooser; then pick glow.qml.
ios.png was captured on a "iPad (4th generation Model A1458)"

Linux capture was on an old X200 Thinkpad (Intel graphics of some sort)

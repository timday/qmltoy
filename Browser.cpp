#include "Browser.h"

#include <QQmlContext>

#include <iostream>

Browser::Browser() {

  setColor(QColor(0,0,0,255));
  setClearBeforeRendering(true);

  setResizeMode(QQuickView::SizeViewToRootObject);

  rootContext()->setContextProperty("browser",this);
}

Browser::~Browser() {
}

void Browser::load(const QUrl& src) {

  std::cerr << "Load of file \"" << src.toString().toLocal8Bit().data() << "\" queued..." << std::endl;

  // NB *must* defer (queue) call to update page content
  // to avoid crash as calling QML page is torn down
  // https://bugreports.qt-project.org/browse/QTBUG-19410
  QMetaObject::invokeMethod(
    this,
    "loadImmediate",
    Qt::QueuedConnection,Q_ARG(QUrl,src)
  );
}

void Browser::loadImmediate(const QUrl& src) {

  std::cerr << "...loading file \"" << src.toString().toLocal8Bit().data() << "\" *now*" << std::endl;

  setSource(src);
  // TODO: Should check status() here!
}

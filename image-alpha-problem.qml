import QtQuick 2.2

Rectangle {
   id: main
   color: '#000000'

   anchors.fill: parent

   property real contentWidthScaling: width/1680.0
   property real contentHeightScaling: height/1050.0
   property real contentScaling: Math.min(contentWidthScaling,contentHeightScaling)

   Rectangle {
     anchors.left: parent.left
     anchors.top: parent.top
     anchors.topMargin: 32*main.contentScaling
     anchors.leftMargin: 32*main.contentScaling
     width: 190.0*main.contentWidthScaling+2.0*(32*main.contentScaling)
     height: 190.0*main.contentHeightScaling+2.0*(32*main.contentScaling)
     color: '#888888'
     visible: true
     smooth: true

     Image {
       anchors.fill: parent
       anchors.margins: 32*main.contentScaling
       source: 'dataicon.png'
       fillMode: Image.PreserveAspectFit
       smooth: true
     }
   }
}

TEMPLATE = app

TARGET = qmltoy

CONFIG -= debug
CONFIG += release
CONFIG += c++11

QT += core gui quick multimedia

SOURCES += $$files(*.cpp)
HEADERS += $$files(*.h)

DESTDIR = build
OBJECTS_DIR = build/obj
MOC_DIR = build/moc

unix:!mac{
  DEFINES += LINUXQUIRKS
  QMAKE_CXXFLAGS += -std=c++11
}

mac {
}

macx {
  DEFINES += MACXQUIRKS
  QMAKE_CXXFLAGS += -std=c++11
}

ios {
  DEFINES += IOSQUIRKS
}

cache()

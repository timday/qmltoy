import QtQuick 2.2
import QtQuick.Controls 1.2

Rectangle {
  id: main
  anchors.fill: parent
  color: '#000000'

  Rectangle {
    id: header
    anchors.top: main.top
    height: parent.height/8
    width: parent.width
    color: '#000088'
    Text {
      anchors.fill: parent
      horizontalAlignment: Text.AlignHCenter
      verticalAlignment: Text.AlignVCenter
      text: "video.qml: "+main.width+"x"+main.height
      color: '#ffff00'
    }
  }

  MyVideo {
    id: output
    anchors.top: header.bottom
    anchors.bottom: footer.top
    width: parent.width
  }

  Rectangle {
    id: footer
    anchors.bottom: main.bottom
    height: parent.height/8
    width: parent.width
    color: '#000088'
    Text {
      anchors.fill: parent
      horizontalAlignment: Text.AlignHCenter
      verticalAlignment: Text.AlignVCenter
      text: "VideoOutput: "+output.width+"x"+output.height+" at "+output.x+"x"+output.y
      color: '#ffff00'
    }

    Button {
      x: 0.1*parent.width-0.5*width
      anchors.verticalCenter: parent.verticalCenter
      id: home
      text: "Index"
      action: Action {onTriggered: browser.load("index.qml");}
    }

    Button {
      x: 0.9*parent.width-0.5*width
      anchors.verticalCenter: parent.verticalCenter
      id: notvideo
      text: "Not video"
      action: Action {onTriggered: browser.load("notvideo.qml");}
    }
  }
  
}

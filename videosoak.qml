import QtQuick 2.2

Rectangle {
  id: main
  anchors.fill: parent
  color: '#000088'

  MyVideo {
    id: output
    anchors.fill: parent
    anchors.margins: 0.1*parent.height
  }

  Timer {
    interval: 1000
    running: true
    onTriggered: browser.load("videosoak.qml")
  }
}

import QtQuick 2.2
import QtMultimedia 5.0

Item {
  VideoOutput {
    fillMode: VideoOutput.PreserveAspectFit
    source: player
  
    // This has helped improve appearance of disappearance of window on iOS in the past: see QTBUG-39385
    // Component.onDestruction: {visible=false;}    
  }

  MediaPlayer {
    id: player
    autoPlay: true
    onStatusChanged: {if (status==MediaPlayer.EndOfMedia) play();}
    source: 'sample_iTunes.mov'
  }

}
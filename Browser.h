#ifndef __qmltoy_browser_h__
#define __qmltoy_browser_h__

#include <QQuickView>

// A QQuickView extended to:
// - expose itself in the hosted QML context as "browser".
// - adds a load slot to switch to another QML file.
class Browser : public QQuickView {
  Q_OBJECT

public:
  Browser();
  ~Browser();

public slots:
  void load(const QUrl&);

private slots:
  void loadImmediate(const QUrl&);
};

#endif

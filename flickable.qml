import QtQuick 2.2

Flickable {
  id: main
  width: 640
  height: 480
  contentWidth: grid.width
  contentHeight: grid.height

  Grid {
    id: grid
    rows: 20
    columns: 20

    Repeater {
      model: grid.rows*grid.columns
      Rectangle {
        width: 100
        height: 100
        color: Qt.rgba(Math.random(),Math.random(),Math.random(),1.0)
      }
    }
  }
}